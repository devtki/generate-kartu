const pkg = require("./package");
// console.log('ENV', process.env.NODE_ENV)
// console.log("test");
import firebase from "firebase";

const configInit = {
  mode: "spa",
  router: {
    base: "/",
    linkExactActiveClass: "active",
  },
  /*
   ** Headers of the page
   */
  server: {
    host: "0.0.0.0",
    port: 3000,
  },
  head: {
    title: "ERP | PT Teknologi Kartu Indonesia",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "ERP | PT Teknologi Kartu Indonesia",
      },
    ],
    link: [
      { rel: "icon", type: "image/png", href: "favicon.png" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700",
      },
      {
        rel: "stylesheet",
        href: "https://use.fontawesome.com/releases/v5.6.3/css/all.css",
        integrity:
          "sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/",
        crossorigin: "anonymous",
      },
      {
        // rel: "stylesheet",
        // href: "https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css",
      },
    ],
    script: [
      {
        // src: "https://unpkg.com/vue-multiselect@2.1.0",
      },
    ],
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },

  /*
   ** Global CSS
   */
  css: ["assets/css/nucleo/css/nucleo.css", "assets/sass/argon.scss"],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "~/plugins/dashboard/dashboard-plugin",
    { src: "~/plugins/dashboard/full-calendar", ssr: false },
    { src: "~/plugins/dashboard/world-map", ssr: false },
    { src: '~/plugins/vue-good-table', ssr: false },
    { src: "~/plugins/vue-datepicker", ssr: false },
    { src: "~/plugins/vue-paginate", ssr: false },
    { src: "~/plugins/vue-multiselect", ssr: false },
    { src: "~/plugins/vue-moment.js", ssr: false },
    { src: "~/plugins/qrcode.js", ssr: false },
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    "@nuxtjs/auth",
    [
      "nuxt-vuex-localstorage",
      {
        mode: "debug",
        localStorage: ["user"],
      },
    ],

    // [
    //   "@nuxtjs/firebase",
    //   {
    //     config: {
    //       apiKey: "AIzaSyBOrM_-ePB97oebbvg86JQfk6lx12rk5aY",
    //       authDomain: "katalis-89754.firebaseapp.com",
    //       databaseURL: "https://katalis-89754.firebaseio.com",
    //       projectId: "katalis-89754",
    //       storageBucket: "katalis-89754.appspot.com",
    //       messagingSenderId: "344174481383",
    //       appId: "1:344174481383:web:9f6846e88681b95a03f742",
    //     },
    //     services: {
    //       auth: true,
    //       firestore: true,
    //       functions: true,
    //       firebase: true,
    //       performance: true,
    //       storage: true,
    //       realtimeDb: true,
    //       messaging: true,
    //       performance: true,
    //       analytics: true,
    //       remoteConfig: true,
    //     },
    //   },
    // ],
  ],

  /*
   ** Axios module configuration
   */
  env: {
    sso_env: "MANUAL", // SSO or MANUAL,
    ssoLoginUrl: "http://192.168.1.102/katalis/login",
    redirectLogin: "/kartu",
  },
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
     baseURL: "https://api.katalis.info/",
    // baseURL: "http://192.168.1.41:5002/",
    // baseURL: "http://192.168.1.111/",
  },
  auth: {
    redirect: {
      logout: "/",
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "erp_login/auth/login",
            method: "post",
            propertyName: "access_token",
          },
          user: false,
          logout: false,
        },
      },
    },
  },

  /*
   ** Build configuration
   */
  build: {
    vendor: ["aframe"],
    transpile: ["vee-validate/dist/rules", "nuxt-vuex-localstorage"],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) { },
    extractCSS: process.env.NODE_ENV === "production",
    babel: {
      plugins: [
        [
          "component",
          {
            libraryName: "element-ui",
            styleLibraryName: "theme-chalk",
          },
        ],
      ],
    },
  },
};
module.exports = configInit;
