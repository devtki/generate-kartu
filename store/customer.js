export const state = () => ({
    customerId: ""
})

export const mutations = {
    SET_CUSTOMER_ID(state, payload) {
        state.customerId = payload
    }
}