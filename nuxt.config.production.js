const pkg = require("./package");
// console.log('ENV', process.env.NODE_ENV)
// console.log("test");

module.exports = {
  mode: "spa",
  router: {
    base: "/",
    linkExactActiveClass: "active",
  },
  /*
   ** Headers of the page
   */
  server: {
    host: "0.0.0.0",
    port: 3000,
  },
  head: {
    title: "CRM PT Teknologi Kartu Indonesia",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "CRM PT Teknologi Kartu Indonesia",
      },
    ],
    link: [
      { rel: "icon", type: "image/png", href: "favicon.png" },
      {
        rel: "stylesheet",
        href:
          "https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700",
      },
      {
        rel: "stylesheet",
        href: "https://use.fontawesome.com/releases/v5.6.3/css/all.css",
        integrity:
          "sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/",
        crossorigin: "anonymous",
      },
    ],
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: "#fff" },

  /*
   ** Global CSS
   */
  css: ["assets/css/nucleo/css/nucleo.css", "assets/sass/argon.scss"],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "~/plugins/dashboard/dashboard-plugin",
    { src: "~/plugins/dashboard/full-calendar", ssr: false },
    { src: "~/plugins/dashboard/world-map", ssr: false },
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    "@nuxtjs/axios",
    "@nuxtjs/pwa",
    "@nuxtjs/auth",
    [
      "nuxt-vuex-localstorage",
      {
        mode: "debug",
        localStorage: ["user"],
      },
    ],
  ],
  /*
   ** Axios module configuration
   */
  env: {
    sso_env: "MANUAL", // SSO or MANUAL,
    ssoLoginUrl: "https://api.katalis.info/katalis/login",
    redirectLogin: "/generator",
  },
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: "https://api.katalis.info/",
  },
  auth: {
    redirect: {
      logout: "/",
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "main/auth/login",
            method: "post",
            propertyName: "access_token",
          },
          user: false,
          logout: false,
        },
      },
    },
  },

  /*
   ** Build configuration
   */
  build: {
    vendor: ["aframe"],
    transpile: ["vee-validate/dist/rules", "nuxt-vuex-localstorage"],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {},
    extractCSS: process.env.NODE_ENV === "production",
    babel: {
      plugins: [
        [
          "component",
          {
            libraryName: "element-ui",
            styleLibraryName: "theme-chalk",
          },
        ],
      ],
    },
  },
};
