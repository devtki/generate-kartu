export default {
    items: [
        {
            isParent: true,
            name: "Data Company",
            solutions: "",
            child: [],
        },
        {
            name: "Data Company",
            url: "/company",
            icon: "ni ni-archive-2 text-green",
            solutions: "",
            child: [],
        },

        // CRM

        {
            isParent: true,
            name: "CRM",
            solutions: "CRM",
            child: [],
        },
        {
            name: "Data Member",
            url: "/crm_member_temp",
            icon: "ni ni-single-02 text-green",
            solutions: "CRM",
            child: [],
        },
        {
            name: "Tampilan Data Kartu",
            url: "/kartu",
            icon: "ni ni-single-02 text-green",
            role: ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_CUSTOMER', 'ROLE_IMPLEMENTATOR', 'ROLE_SALES', 'ROLE_ADMIN_PARTNER'],
            solutions: "CRM_KATALIS",
            child: [],
        },



    ],
};
